import { Fragment } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
// Import necessary components from react-bootstrap
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import { BsFillCartCheckFill } from "react-icons/bs";

// AppNavbar component
export default function AppNavbar(){

	return(
		
		<Container id="admin-bg" fluid>
					<Navbar id="Sidebar" bg="primary" variant="dark">
					  <Navbar.Brand className="m-1" as={NavLink} to="/login" exact href="#home">
					    <BsFillCartCheckFill size={40} />{' '}
					    Dashboard
					  </Navbar.Brand>
					  <Nav id="Sidebar-Body" className="me-auto">
					        <Nav.Link href="#home">Manage Products</Nav.Link>
					        <Nav.Link href="#features">Features</Nav.Link>
					        <Nav.Link href="#pricing">Pricing</Nav.Link>
					      </Nav>
					</Navbar>
					<div id="content-bg">
						<h1 className="m-5" id="Banner-Title">Manage Products</h1>
						<div>
							<Button id="Add-admin" variant="success">Add Product</Button>	
						</div>
						<div>
							<div>
								<Row xs={1} md={1}>
								  {Array.from({ length: 8 }).map((_, idx) => (
								    <Col>
								      <div id="Cards-Admin">
								      	<Row id="Admin-Cards">
								      		<Col>
								      			ProductID
								      		</Col>
								      		<Col>
								      			ProductName
								      		</Col>
								      		<Col>
								      			Price
								      		</Col>
								      		<Col>
								      			Date
								      		</Col>
								      		<Col>
								      			<Button variant="primary">Udate</Button>
								      		</Col>
								      		<Col>
								      			<Button variant="danger">Delete</Button>
								      		</Col>
								      	</Row>
								      </div>
								    </Col>
								  ))}
								</Row>
							</div>
						</div>
					</div>
		</Container>
	)
}

