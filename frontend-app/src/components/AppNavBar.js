import { Fragment } from 'react';
import Products from '../pages/Products.js'
import Sidebar from '../components/Sidebar.js'
// Import necessary components from react-bootstrap
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import { BsFillCartCheckFill } from "react-icons/bs";

// AppNavbar component
export default function AppNavbar(){

	return(
		<Navbar bg="primary" id="AppNavbar" variant="dark">
		  <Navbar.Brand as={NavLink} to="/" exact href="#home">
		    <BsFillCartCheckFill size={40} />{' '}
		    Cartee
		  </Navbar.Brand>
		  <Navbar.Collapse id="basic-navbar-nav">
			    <Nav>
			      <Nav.Link as={NavLink} to="/products-page" exact>Products Page</Nav.Link>
			      <Nav.Link as={NavLink} to="/admin" exact>Admin</Nav.Link>
			    </Nav>
		   </Navbar.Collapse>
		</Navbar>
	)
}
