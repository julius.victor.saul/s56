import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import Form from 'react-bootstrap/Form'
import { BsFillCartCheckFill } from "react-icons/bs";
import '../App.css'
import '../font/SFPRODISPLAYBOLD.OTF'; 

export default function Admin() {
	return (
		<Container id="admin-bg" fluid>
			<div id="Banner">
				<BsFillCartCheckFill size={400} />
				<h1 id="Banner-Title">Shop Here Now!</h1>
			</div>
			<div>
				<Row className="m-0 p-0">
					<Col lg={2}>
						<div id="Filter">
							<h1>Filter By</h1>
							<Form.Select aria-label="Default select example">
							  	<option>Brand</option>
							  	<option value="1">Color</option>
							  	<option value="2">Size</option>
							</Form.Select>
						</div>						
					</Col>
					<Row xs={1} md={4} id="Product-Cards">
					  {Array.from({ length: 12 }).map((_, idx) => (
					    <Col>
					      <Card id="Cards">
					        <BsFillCartCheckFill id="Card-Image" size={200} />
					        <Card.Body>
					          <Card.Title>Product Name</Card.Title>
					          <Card.Text>
					            Price : 2000
					          </Card.Text>
					        </Card.Body>
					      </Card>
					    </Col>
					  ))}
					</Row>
				</Row>
			</div>
		</Container>
	)
}

