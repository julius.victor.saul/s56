import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import Form from 'react-bootstrap/Form'
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import Login from '../pages/Login.js'
import { BsFillCartCheckFill } from "react-icons/bs";
import '../App.css'
import '../font/SFPRODISPLAYBOLD.OTF'; 
import Swal from 'sweetalert2';

export default function Register() {

	const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [mobile, setMobile] = useState('');
    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
    // Validate to enable submit button when all fields are populated and both passwords match
    	if ((email !== '' && mobile !== ''  && password1 !== '' && password2 !== '') && (password1 === password2))
    	{
    	  setIsActive(true);
    	}
    	else{
    	  setIsActive(false);
    	}
  	}, [ email, mobile, password1, password2])

    function registerUser(e){
    	e.preventDefault();
    }
    
    function alert() {
    	Swal.fire({
    	  icon: 'success',
    	  iconColor: 'blue',
    	  title: 'Succesfully Registered',
    	  text: 'Click OK to proceed to Login'
    	})
    }





	return (
		<Container id="row-bg" fluid>
			<Row>
				<Col lg={8}>
					<div id="Logo-Login-div">
						<BsFillCartCheckFill size={400} />
					</div>
					<div id="Title-Login-div">
						<h1 id="Title">Cartee</h1>
					</div>
				</Col>
				<Col lg={4}>
					<div id="Form-div">
						<Form id="Form" onSubmit={(e) => registerUser(e)}>
							<h1 id="Form-Title">Register</h1>
						  	<Form.Group className="mb-2" controlId="formRegister">
						    	<Form.Label>Email address</Form.Label>
						    	<Form.Control 
						    	type="email" 
						    	value={email} 
						    	placeholder="Enter email"
						    	onChange={ e => setEmail(e.target.value)}
						    	/>
						  	</Form.Group>

						  	<Form.Group className="mb-3" controlId="formRegister">
						    	<Form.Label>Mobile No.</Form.Label>
						    	<Form.Control 
						    	type="text" 
						    	value={mobile} 
						    	placeholder="Mobile Number"
						    	onChange={ e => setMobile(e.target.value)}
						    	/>
						  	</Form.Group>

						  	<Form.Group className="mb-3" controlId="formRegister">
						    	<Form.Label>Password</Form.Label>
						    	<Form.Control 
						    	type="password" 
						    	value={password1} 
						    	placeholder="Password"
						    	onChange={ e => setPassword1(e.target.value)} 
						    	/>
						  	</Form.Group>
						  	<Form.Group className="mb-3" controlId="formRegister">
						    	<Form.Label>Verify Password</Form.Label>
						    	<Form.Control 
						    	type="password" 
						    	value={password2} 
						    	placeholder="Verify Password"
						    	onChange={ e => setPassword2(e.target.value)} 
						    	/>
						  	</Form.Group>
						  	{ isActive ?
						  		<Link to={'Login'}>
						  			<Button onClick={alert} className="mt-4" to={Login} id="Form-Button" type="submit">
						  			  	Register
						  			</Button>
						  		</Link>
						  		:
						  		// Disabled state
						  		<Button className="mt-4" id="Form-Button" variant="danger" type="submit" disabled>
						  			  Register
						  		</Button>
						  	}
				
						</Form>
					</div>
				</Col>
			</Row>
		</Container>
	)
}

