import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import Form from 'react-bootstrap/Form'
import { BsFillCartCheckFill } from "react-icons/bs";
import '../App.css'
import '../font/SFPRODISPLAYBOLD.OTF'; 

export default function Login() {




	return (
		<Container id="row-bg" fluid>
			<Row>
				<Col lg={8}>
					<div id="Logo-Login-div">
						<BsFillCartCheckFill size={400} />
					</div>
					<div id="Title-Login-div">
						<h1 id="Title">Cartee</h1>
					</div>
				</Col>
				<Col lg={4}>
					<div id="Form-Login-div" >
						<Form id="Form">
							<h1 id="Form-Title">Login</h1>
						  	<Form.Group className="mb-2" controlId="formRegister">
						    	<Form.Label>Email address</Form.Label>
						    	<Form.Control type="email" placeholder="Enter email" />
						  	</Form.Group>

						  	<Form.Group className="mb-3" controlId="formRegister">
						    	<Form.Label>Password</Form.Label>
						    	<Form.Control type="password" placeholder="Password" />
						  	</Form.Group>

						  	<Button className="mt-4" id="Form-Button" type="submit">
						    	Login
						  	</Button>
						</Form>
					</div>
				</Col>
			</Row>
		</Container>
	)
}

