import AppNavBar from './components/AppNavBar.js'
import Sidebar from './components/Sidebar.js'
import Login from './pages/Login.js'
import Register from './pages/Register.js'
import Products from './pages/Products.js'
import Admin from './pages/Admin.js'
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import './App.css';

function App() {
  return (
    <Router>
      <AppNavBar />
            <Routes>
              <Route exact path="/" element={<Register/>} />
              <Route exact path="/login" element={<Login/>} />
              <Route exact path="/products-page" element={<Products/>} />
              <Route exact path="/admin" element={<Sidebar/>} />
            </Routes>
      
    </Router>
  )
}

export default App;
  